#!/bin/bash
i=1
exp=0
cont_exp=1
players="./players.csv"                                                                                                                                                ennemies="./enemies.csv"                                                                                                                                               bosses="./bosses.csv"

function select_players(){
    rdm=$((RANDOM % 100))
    x=0
    if (($rdm < 50));then
        char=$(head -n 2 $1 | tail -n +2 |cut -d"," -f2)
        x=2
    elif (($rdm < 80));then
        char=$(head -n 3 $1 | tail -n +3 |cut -d"," -f2)
        x=3
    elif (($rdm <95));then
        char=$(head -n 4 $1 | tail -n +4 |cut -d"," -f2)
        x=4
    elif (($rdm < 99));then
        char=$(head -n 5 $1 | tail -n +5 |cut -d"," -f2)
        x=5
    elif (($rdm <100));then
        char=$(head -n 6 $1 | tail -n +6 |cut -d"," -f2)
        x=6
    fi
    HP_p=$(head -n $x $1 | tail -n +$x |cut -d"," -f3)
    STR_p=$(head -n $x $1 | tail -n +$x |cut -d"," -f5)
    }

function select_ennemies(){
    rdm=$((RANDOM % 300))
    x=0
    if (($rdm < 15));then
        char_m=$(head -n 2 $1 | tail -n +2 |cut -d"," -f2)
        x=2
    elif (($rdm < 16));then
        char_m=$(head -n 3 $1 | tail -n +3 |cut -d"," -f2)
        x=3
    elif (($rdm <20));then
        char_m=$(head -n 4 $1 | tail -n +4 |cut -d"," -f2)
        x=4
    elif (($rdm < 24));then
        char_m=$(head -n 5 $1 | tail -n +5 |cut -d"," -f2)
        x=5
    elif (($rdm <25));then
        char_m=$(head -n 6 $1 | tail -n +6 |cut -d"," -f2)
        x=6
    elif (($rdm <55));then
        char_m=$(head -n 7 $1 | tail -n +7 |cut -d"," -f2)
        x=7
    elif (($rdm <105));then
        char_m=$(head -n 8 $1 | tail -n +8 |cut -d"," -f2)
        x=8
    elif (($rdm <120));then
        char_m=$(head -n 9 $1 | tail -n +9 |cut -d"," -f2)
        x=9
    elif (($rdm <150));then
        char_m=$(head -n 10 $1 | tail -n +10 |cut -d"," -f2)
        x=10
    elif (($rdm <200));then
        char_m=$(head -n 11 $1 | tail -n +11 |cut -d"," -f2)
        x=11
    elif (($rdm <250));then
        char_e=$(head -n 12 $1 | tail -n +12 |cut -d"," -f2)
        x=12
    elif (($rdm <300));then
        char_m=$(head -n 13 $1 | tail -n +13 |cut -d"," -f2)
        x=13
    fi
    HP_m=$(head -n $x $1 | tail -n +$x |cut -d"," -f3)
    STR_m=$(head -n $x $1 | tail -n +$x |cut -d"," -f5)  

}

function select_bosses(){
    rdm=$((RANDOM % 119))
    x=0
    if (($rdm < 50));then
        char_b=$(head -n 2 $1 | tail -n +2 |cut -d"," -f2)
        x=2
    elif (($rdm < 65));then
        char_b=$(head -n 3 $1 | tail -n +3 |cut -d"," -f2)
        x=3
    elif (($rdm <69));then
        char_b=$(head -n 4 $1 | tail -n +4 |cut -d"," -f2)
        x=4
    elif (($rdm < 70));then
        char_b=$(head -n 5 $1 | tail -n +5 |cut -d"," -f2)
        x=5
    elif (($rdm <85));then
        char_b=$(head -n 6 $1 | tail -n +6 |cut -d"," -f2)
        x=6
    elif (($rdm <89));then
        char_b=$(head -n 7 $1 | tail -n +7 |cut -d"," -f2)
        x=7
    elif (($rdm <119));then
        char_b=$(head -n 8 $1 | tail -n +8 |cut -d"," -f2)
        x=8
    fi

    HP_b=$(head -n $x $1 | tail -n +$x |cut -d"," -f3)
    STR_b=$(head -n $x $1 | tail -n +$x |cut -d"," -f5)


}
       
function battle(){

    char_e=$1
    HP_e=$2
    STR_e=$3
    HP_e_Max=$HP_e
    echo  ========================= FLOOR $i ======================================
   
    while (($HP_p >0 && $HP_e >0 ))
    do
        option=0
        p=0
        echo $char_e
        echo HP: $HP_e / $HP_e_Max
        echo $char
        echo HP: $HP_p / $HP_p_Max
        echo -----option------
        while(($option !=1 && $option !=2 && $option !=3 && $option !=4))
        do
            echo -------------------- Link"'"s turn ------------------------
            echo 1.Attack  2.Heal   3.escape    4.protect
            echo You encounter a $char_e
            read option 
            case $option in
                1)  HP_e=$(($HP_e - $STR_p))
                    echo $char attacked and made $STR_p;;
                2)  if (($HP_p <= $HP_p_Max/2))
                    then
                        HP_p=$(($HP_p + $HP_p_Max/2))
                        echo $char Healed -> $char"'"s HP : $HP_p/$HP_p_Max
                    else
                        HP=60
                        echo $char Healed -> $char"'"s HP : $HP_p /$HP_p_Max
                    fi;;
                3)  echo You are a chiken 
                    i=1
                    exit;;
                4)  p=1;;
                *) echo choose 1 or 2 or 3 or 4
            esac
        done

        if (($HP_e > 0));then 
            echo --------------------- Bokoblin"'"s turn -------------------------
            if(($p==1));then
                echo you recieved $STR_e/2 
                HP_p=$(($HP_p -$STR_e/2))
                else
                     echo  you recieved $STR_p damage
                     HP_p=$(($HP_p - $STR_e));
            fi
        fi               
    done
    if(($HP_p > 0));then
        echo $char_e
        echo HP: $HP_e / $HP_e_Max
        echo $char
        echo HP: $HP_p / $HP_p_Max
        echo  $char_e is dead
        echo you can move to  the next floor
        XP=$((15 + RANDOM % 35))
        exp=$(($exp + $XP))
        if (($exp>=100*$cont_exp));then
            level_up
            exp=$(($exp%100*$cont_exp))
            cont_exp=$(($cont_exp +1 ))
                fi
    else echo you lost shit happens i guess 
    fi
}

function level_up(){
    echo  ============== congrats you leveled up ==================
    gain_hp=$((5+ RANDOM % 5))
    gain_str=$((RANDOM % 3))
    HP_p_Max=$(($HP_p_Max+$gain_hp))
    STR_p=$(($STR_p+$gain_str))
    level=$(($cont_exp+1))
    echo your new stats:
    echo $char
    echo level : $level
    echo HP : $HP_p_Max
    echo str : $STR_p
    }

select_players $players
select_bosses $bosses
HP_p_Max=$HP_p

while(($i<10 && $HP_p > 0))
do
    select_ennemies $ennemies
    battle $char_m $HP_m $STR_m
    i=$(($i+1))
done

if(($HP_p > 0));then
    echo ================= LAST BOSS ============================
    battle $char_b $HP_b $STR_b
    fi
